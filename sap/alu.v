/*
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
`ifdef _MUX_V
`else
  `define _MUX_V
  `include "mux.v"
`endif
`ifdef _ADDER_V
`else
  `define _ADDER_V
  `include "adder.v"
`endif

module alu
(
  enable_alu,
  addsub,
  a,
  b,
  carry,
  alu_out
);

  input enable_alu;
  input addsub;
  input [3:0] a;
  input [3:0] b;
  output carry;
  output [3:0] alu_out;

  wire enable_alu;
  wire addsub;
  wire [3:0] a;
  wire [3:0] b;
  wire carry;
  wire [3:0] alu_out;

  wire [3:0] az;
  wire [3:0] a_or_az;
  wire ci;
  wire [3:0] sum;
  wire [2:0] cout;

  assign az = ~a;
  assign alu_out = (enable_alu) ? sum : 4'bz;
  
  mux U_mux0
  (
    .in_0 (a[0]),
    .in_1 (az[0]),
    .sel (addsub),
    .out (a_or_az[0])
  );

  mux U_mux1
  (
    .in_0 (a[1]),
    .in_1 (az[1]),
    .sel (addsub),
    .out (a_or_az[1])
  );

  mux U_mux2
  (
    .in_0 (a[2]),
    .in_1 (az[2]),
    .sel (addsub),
    .out (a_or_az[2])
  );

  mux U_mux3
  (
    .in_0 (a[3]),
    .in_1 (az[3]),
    .sel (addsub),
    .out (a_or_az[3])
  );

  mux U_muxci
  (
    .in_0 (1'b0),
    .in_1 (1'b1),
    .sel (addsub),
    .out (ci)
  );

  adder U_adder0
  (
    .a (b[0]),
    .b (a_or_az[0]),
    .ci (ci),
    .s (sum[0]),
    .co (cout[0])
  );

  adder U_adder1
  (
    .a (b[1]),
    .b (a_or_az[1]),
    .ci (cout[0]),
    .s (sum[1]),
    .co (cout[1])
  );

  adder U_adder2
  (
    .a (b[2]),
    .b (a_or_az[2]),
    .ci (cout[1]),
    .s (sum[2]),
    .co (cout[2])
  );

  adder U_adder3
  (
    .a (b[3]),
    .b (a_or_az[3]),
    .ci (cout[2]),
    .s (sum[3]),
    .co (carry)
  );

endmodule
