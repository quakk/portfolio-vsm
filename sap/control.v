/*
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
module control
(
  phase,
  instr,
  readmem,
  progcount,
  load_out,
  load_instr,
  load_b,
  load_a,
  enable_instr,
  enable_in,
  enable_alu,
  enable_a,
  addsub
);

  input [3:0] phase;
  input [3:0] instr;
  output readmem;
  output progcount;
  output load_out;
  output load_instr;
  output load_b;
  output load_a;
  output enable_instr;
  output enable_in;
  output enable_alu;
  output enable_a;
  output addsub;

  wire [3:0] phase;
  wire [3:0] instr;
  wire readmem;
  wire progcount;
  wire load_out;
  wire load_inst;
  wire load_b;
  wire load_a;
  wire enable_instr;
  wire enable_in;
  wire enable_alu;
  wire enable_a;
  wire addsub;

  wire [3:0] instrz;
  assign instrz = ~instr;

  wire nop;
  wire add;
  wire sub;
  wire in;
  wire out;
  wire lda;

  assign nop = instrz[3] & instrz[2] & instrz[1] & instrz[0];
  assign add = instrz[3] & instrz[2] & instrz[1] & instr[0];
  assign sub = instrz[3] & instrz[2] & instr[1] & instrz[0];
  assign in = instrz[3] & instrz[2] & instr[1] & instr[0];
  assign out = instrz[3] & instr[2] & instrz[1] & instrz[0];
  assign lda = instrz[3] & instr[2] & instrz[1] & instr[0];

  assign readmem = phase[0];
  assign load_instr = phase[0];
  assign progcount = phase[1];
  assign addsub = add;
  assign enable_instr = phase[1] | (add | sub | lda) & phase[2];
  assign load_b = (add | sub) & phase[2]; 
  assign enable_alu = (add | sub) & phase[3];
  assign load_a = (add | sub) & phase[3] | (in | lda) & phase[2];
  assign enable_in = in & phase[2];
  assign load_out = out & phase[2];

  assign enable_a = (in | out) & phase[2];

endmodule
