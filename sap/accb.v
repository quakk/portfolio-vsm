/*
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
`ifdef _DFF_V
`else
  `define _DFF_V
  `include "dff.v"
`endif

module accb
(
  rst,
  clk,
  load_b,
  b_in,
  alu_out,
);

  input rst;
  input clk;
  input load_b;
  input [3:0] b_in;
  output [3:0] alu_out;

  wire rst;
  wire clk;
  wire load_b;
  wire [3:0] b_in;
  wire [3:0] alu_out;

  wire rstz;
  wire clken;
  
  assign rstz = ~rst;
  assign clken = clk & load_b;

  dff U_dff0
  (
    .rst (rstz),
    .clk (clken),
    .d (b_in[0]),
    .q (alu_out[0]),
    .qz ()
  );

  dff U_dff1
  (
    .rst (rstz),
    .clk (clken),
    .d (b_in[1]),
    .q (alu_out[1]),
    .qz ()
  );

  dff U_dff2
  (
    .rst (rstz),
    .clk (clken),
    .d (b_in[2]),
    .q (alu_out[2]),
    .qz ()
  );

  dff U_dff3
  (
    .rst (rstz),
    .clk (clken),
    .d (b_in[3]),
    .q (alu_out[3]),
    .qz ()
  );

endmodule
