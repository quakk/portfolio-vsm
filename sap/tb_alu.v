`include "alu.v"

module tb_alu();

  reg enable_alu;
  reg addsub;
  reg [3:0] a;
  reg [3:0] b;
  wire co;
  wire [3:0] s;

  integer i;
  integer j;
  integer m;
  integer n;
  reg [4:0] expected;

  initial
  begin
    enable_alu = 0;

    // 0 for addition, 1 for subtraction
    addsub = 0;
    
    for (m = 0;  m <= 1;  m = m + 1)
    begin
      enable_alu = m;
//      #10;
      for (n = 0;  n <= 1;  n = n + 1)
      begin
        addsub = n;
//        #10;
        for (i = 0;  i < 16;  i = i + 1)
        begin
          a <= i;
          for (j = 0;  j < 16;  j = j + 1)
          begin
            b <= j;
            if (addsub)
            begin
              expected <= j - i;
              // don't care about carry for subtraction
              #5 if (expected[3:0] != s[3:0])
              begin
                $display("-E- %d (s):  Expected %d;  Observed %d", $time, expected[3:0], s[3:0]);
              end
            end
            else
            begin
              expected <= j + i;
              #5 if (expected[3:0] != s[3:0])
              begin
                $display("-E- %d (s):  Expected %d;  Observed %d", $time, expected[3:0], s[3:0]);
              end
              #5 if (expected[4] != co)
              begin
                $display("-E- %d (co):  Expected %d;  Observed %d", $time, expected[4], co);
              end
            end
            #20;
          end
        end
      end
    end

    #50 $finish;
  end

  alu U_alu
  (
    .enable_alu (enable_alu),
    .addsub (addsub),
    .a (a),
    .b (b),
    .carry (co),
    .alu_out (s)
  );
  
  initial
  begin
    $dumpfile("out/tb_alu.lxt");
    $dumpvars(0,tb_alu);
  end

endmodule
