/*
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
//`include "dff.v"

module out
(
  rst,
  clk,
  load,
  data_in,  
  data_out
);

  input rst;
  input clk;
  input load;
  input [3:0] data_in;
  output [3:0] data_out; 

  wire rst;
  wire clk;
  wire load;
  wire [3:0] data_in;
  wire [3:0] data_out;

  wire rstz;
  wire clkenz;

  assign rstz = ~rst;
  assign clkenz = ~(load & clk);

  dff U_dff0
  (
    .rst (rstz),
    .clk (clkenz),
    .d (data_in[0]),
    .q (data_out[0]),
    .qz ()
  );

  dff U_dff1
  (
    .rst (rstz),
    .clk (clkenz),
    .d (data_in[1]),
    .q (data_out[1]),
    .qz ()
  );

  dff U_dff2
  (
    .rst (rstz),
    .clk (clkenz),
    .d (data_in[2]),
    .q (data_out[2]),
    .qz ()
  );

  dff U_dff3
  (
    .rst (rstz),
    .clk (clkenz),
    .d (data_in[3]),
    .q (data_out[3]),
    .qz ()
  );

endmodule
