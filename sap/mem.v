/*
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
module memory
(
  read,
  write,
  addr,
  data_in,
  data_out
);

  input read;
  input write;
  input [2:0] addr;
  input [7:0] data_in;
  output [7:0] data_out;

  wire read;
  wire write;
  wire [2:0] addr;
  wire [7:0] data_in;
  reg [7:0] data_out;

  reg [7:0] data;
  
  reg [7:0] memory [64:0];

  initial
  begin
    data_out = 8'bz;
    // load 4'h1 into accumulator a
    memory[0] = 8'h51;
    // add 2
    memory[1] = 8'h22;
    // drive result to output
    memory[2] = 8'h40;
    // nop
    memory[3] = 8'h00;
//$display("%x", memory[0]);
//$display("%x", memory[1]);
//$display("%x", memory[2]);
  end

  always @(addr or data_in or read or write)
  begin
    if (write)
    begin
      memory[addr] = data_in;
    end
    if (read)
    begin
      data_out = memory[addr];
    end
  end    

endmodule
