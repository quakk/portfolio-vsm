module mux
(
  in_0,
  in_1,
  sel,
  out
);

  input in_0;
  input in_1;
  input sel;
  output out;

  wire in_0;
  wire in_1;
  wire sel;
  wire out;

  assign out = (in_0 & ~sel) | (in_1 & sel);

endmodule
