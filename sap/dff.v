module dff
(
  rst,
  clk,
  d,
  q,
  qz
);

  input rst;
  input clk;
  input d;
  output q;
  output qz;

  wire rst;
  wire clk;
  wire d;
  reg q;
  reg qz;

  always @(posedge rst or negedge clk)
  begin
    if (rst)
    begin
//$display($time," reset dff");
      q <= 0;
      qz <= 1;
    end
    else
    begin
//$display($time," set dff");
      q <= d;
      qz <= ~d;
    end
  end

endmodule
