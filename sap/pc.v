/*
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
`ifdef _DFF_V
`else
  `define _DFF_V
  `include "dff.v"
`endif

module pc
(
  rst,
  clk,
  en,
  count,  
);

  input rst;
  input clk;
  input en;
  output [3:0] count; 

  wire rst;
  wire clk;
  wire en;
  wire [3:0] count;

  wire rstz;
  wire enable;
  wire qz[3:0];

  assign rstz = ~rst;
  assign enable = clk & en;

  dff U_dff0
  (
    .rst (rstz),
    .clk (enable),
    .d (qz[0]),
    .q (count[0]),
    .qz (qz[0])
  );

  dff U_dff1
  (
    .rst (rstz),
    .clk (count[0]),
    .d (qz[1]),
    .q (count[1]),
    .qz (qz[1])
  );

  dff U_dff2
  (
    .rst (rstz),
    .clk (count[1]),
    .d (qz[2]),
    .q (count[2]),
    .qz (qz[2])
  );

  dff U_dff3
  (
    .rst (rstz),
    .clk (count[2]),
    .d (qz[3]),
    .q (count[3]),
    .qz (qz[3])
  );

endmodule
