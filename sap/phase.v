/*
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
`ifdef _DFF_V
`else
  `define _DFF_V
  `include "dff.v"
`endif

module phase
(
  rstz,
  clk,
  phase
);

  input rstz;
  input clk;
  output [3:0] phase; 

  wire rstz;
  wire clk;
  wire [3:0] phase;

  wire [3:0] q;
  wire [3:0] qz;

  wire rst;
  assign rst = ~rstz;

  assign phase[0] = q[3] ^ qz[0];
  assign phase[1] = q[0] ^ q[1];
  assign phase[2] = q[1] ^ q[2];
  assign phase[3] = q[2] ^ q[3];

  dff U_dff0
  (
    .rst (rst),
    .clk (clk),
    .d (qz[3]),
    .q (q[0]),
    .qz (qz[0])
  );

  dff U_dff1
  (
    .rst (rst),
    .clk (clk),
    .d (q[0]),
    .q (q[1]),
    .qz (qz[1])
  );

  dff U_dff2
  (
    .rst (rst),
    .clk (clk),
    .d (q[1]),
    .q (q[2]),
    .qz (qz[2])
  );

  dff U_dff3
  (
    .rst (rst),
    .clk (clk),
    .d (q[2]),
    .q (q[3]),
    .qz (qz[3])
  );

endmodule
