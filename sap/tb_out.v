`include "out.v"

module tb_out();

  reg rst;
  reg clk;
  reg load_out;
  reg [3:0] data_in;
  wire [3:0] data_out;

  out U_out
  (
    .rst (rst),
    .clk (clk),
    .load (load_out),
    .data_in (data_in),
    .data_out (data_out)
  );

  initial
  begin
    rst <= 0;
    clk <= 0;
    load_out <= 0;
    data_in <= 4'h0;

    #50 rst <= 0;
    #50 rst = 1;

    #100 load_out <= 1;
    #20 data_in = 4'h1;
    #20 data_in = 4'h2;
    #20 data_in = 4'h3;
    #20 data_in = 4'h4;
    #20 data_in = 4'h5;
    #20 data_in = 4'h6;
    #20 data_in = 4'h7;
    #20 data_in = 4'h8;
    #20 data_in = 4'h9;
    #20 data_in = 4'ha;
    #20 data_in = 4'hb;
    #20 data_in = 4'hc;
    #20 data_in = 4'hd;
    #20 data_in = 4'he;
    #20 data_in = 4'hf;

    #20 data_in = 4'h0;
    #20 load_out <= 0;
    #20 data_in = 4'h1;
    #20 data_in = 4'h2;
    #20 data_in = 4'h3;
    #20 data_in = 4'h4;
    #20 data_in = 4'h5;
    #20 data_in = 4'h6;
    #20 data_in = 4'h7;
    #20 data_in = 4'h8;
    #20 data_in = 4'h9;
    #20 data_in = 4'ha;
    #20 data_in = 4'hb;
    #20 data_in = 4'hc;
    #20 data_in = 4'hd;
    #20 data_in = 4'he;
    #20 data_in = 4'hf;

    #50 $finish;
  end

  always
  begin
    #10 clk = ~clk;
  end

  initial
  begin
    $dumpfile("out/tb_out.lxt");
    $dumpvars(0,tb_out);
  end

endmodule
