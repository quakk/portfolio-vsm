`include "control.v"

module tb_control();

  reg clk;
  reg [3:0] phase;
  reg [3:0] instr;
  wire readmem;
  wire progcount;
  wire load_out;
  wire load_instr;
  wire load_b;
  wire load_a;
  wire enable_instr;
  wire enable_in;
  wire enable_alu;
  wire enable_a;
  wire addsub;

  control U_control
  (
    .phase (phase),
    .instr (instr),
    .readmem (readmem),
    .progcount (progcount),
    .load_out (load_out),
    .load_instr (load_instr),
    .load_b (load_b),
    .load_a (load_a),
    .enable_instr (enable_instr),
    .enable_in (enable_in),
    .enable_alu (enable_alu),
    .enable_a (enable_a),
    .addsub (addsub)
  );

  initial
  begin
    clk = 0;
    phase = 4'b0001;
    instr = 4'b0000;

    #80 instr = 4'b0001;
    #80 instr = 4'b0010;
    #80 instr = 4'b0011;
    #80 instr = 4'b0100;
    #80 instr = 4'b0101;
    #80 instr = 4'b0110;
    #80 instr = 4'b0111;
    #80 instr = 4'b1000;
    #80 instr = 4'b1001;
    #80 instr = 4'b1010;
    #80 instr = 4'b1011;
    #80 instr = 4'b1100;
    #80 instr = 4'b1101;
    #80 instr = 4'b1110;
    #80 instr = 4'b1111;
    
    #50 $finish;
  end

  always
  begin
    #20 phase[0] = ~phase[0];
    phase[1] = ~phase[1];
    #20 phase[1] = ~phase[1];
    phase[2] = ~phase[2];
    #20 phase[2] = ~phase[2];
    phase[3] = ~phase[3];
    #20 phase[3] = ~phase[3];
    phase[0] = ~phase[0];
  end

  always
  begin
    #10 clk = ~clk;
  end

  initial
  begin
    $dumpfile("out/tb_control.lxt");
    $dumpvars(0,tb_control);
  end

endmodule  
