/*
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
`ifdef _DFF_V
`else
  `define _DFF_V
  `include "dff.v"
`endif

module ir
(
  rst,
  clk,
  enable_ir,
  load_ir,
  instr_in,
  data_in,
  instr_out,
  data_out,
);

  input rst;
  input clk;
  input enable_ir;
  input load_ir;
  input [3:0] instr_in;
  input [3:0] data_in;
  output [3:0] instr_out;
  output [3:0] data_out;

  wire rst;
  wire clk;
  wire enable_ir;
  wire load_ir;
  wire [3:0] instr_in;
  wire [3:0] data_in;
  wire [3:0] instr_out;
  wire [3:0] data_out;

  wire rstz;
  wire clken;
  wire [3:0] dataz;
  
  assign rstz = ~rst;
  assign clken = clk & load_ir;

  assign data_out = (enable_ir) ? ~dataz : 4'bz;

  dff U_idff0
  (
    .rst (rstz),
    .clk (clken),
    .d (instr_in[0]),
    .q (instr_out[0]),
    .qz ()
  );

  dff U_idff1
  (
    .rst (rstz),
    .clk (clken),
    .d (instr_in[1]),
    .q (instr_out[1]),
    .qz ()
  );

  dff U_idff2
  (
    .rst (rstz),
    .clk (clken),
    .d (instr_in[2]),
    .q (instr_out[2]),
    .qz ()
  );

  dff U_idff3
  (
    .rst (rstz),
    .clk (clken),
    .d (instr_in[3]),
    .q (instr_out[3]),
    .qz ()
  );

  dff U_odff0
  (
    .rst (rstz),
    .clk (clken),
    .d (data_in[0]),
    .q (),
    .qz (dataz[0])
  );

  dff U_odff1
  (
    .rst (rstz),
    .clk (clken),
    .d (data_in[1]),
    .q (),
    .qz (dataz[1])
  );

  dff U_odff2
  (
    .rst (rstz),
    .clk (clken),
    .d (data_in[2]),
    .q (),
    .qz (dataz[2])
  );

  dff U_odff3
  (
    .rst (rstz),
    .clk (clken),
    .d (data_in[3]),
    .q (),
    .qz (dataz[3])
  );

endmodule
