`include "ir.v"

module tb_ir();

  reg enable_ir;
  reg rst;
  reg load_ir;
  reg clk;
  reg [7:0] data;

  wire [3:0] instr;
  wire [3:0] bus;

  ir U_ir
  (
    .rst (rst),
    .clk (clk),
    .enable_ir (enable_ir),
    .load_ir (load_ir),
    .instr_in (data[3:0]),
    .data_in (data[7:4]),
    .instr_out (instr),
    .data_out (bus)
  );

  initial
  begin
    rst <= 1;
    clk <= 0;
    enable_ir <= 0;
    load_ir <= 0;
    data = 7'b0;

    #5 rst = 0;
    #50 rst = 1;

    #20 data = 8'ha5;
    load_ir = 1;

    #20 load_ir = 0;
    data = 8'h00;
    #20 enable_ir = 1;
    #20 enable_ir = 0;

    #20 data = 8'h3c;
    load_ir = 1;

    #20 load_ir = 0;
    data = 8'h00;
    #20 enable_ir = 1;
    #20 enable_ir = 0;

    #50 $finish;
  end

  always
  begin
    #10 clk = ~clk;
  end

  initial
  begin
    $dumpfile("out/tb_ir.lxt");
    $dumpvars(0,tb_ir);
  end

endmodule  
