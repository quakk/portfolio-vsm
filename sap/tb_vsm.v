`include "vsm.v"

module tb_vsm();

  reg rst;
  reg clk;
  reg writemem;
  reg [7:0] memin;
  reg [3:0] din;
  wire [3:0] bus;
  wire [3:0] out;

  integer period = 20;

  initial
  begin
    rst = 0;
    clk = 0;
    writemem = 0;
    memin = 7'h00;
    din = 4'b0;
    
    // memory initialised explicitly in mem.v module

    // reset and let 'er rip
    #(4 * period) rst = 0;
    #(2 * period + 1) rst = 1;

    #(16 * period) rst = 0;
    #(4 * period) $finish;
  end

  always
  begin
    #(period/2) clk = ~clk;
  end

  initial
  begin
    $dumpfile("out/tb_vsm.lxt");
    $dumpvars(0,tb_vsm);
  end

  vsm U_vsm
  (
    .rst (rst),
    .clk (clk),
    .writemem (writemem),
    .memin (memin),
    .din (din),
    .bus (bus),
    .out (out)
  );

endmodule
