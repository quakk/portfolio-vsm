/*
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
`ifdef _DFF_V
`else
  `define _DFF_V
  `include "dff.v"
`endif

module acca
(
  rst,
  clk,
  enable_a,
  load_a,
  a_in,
  alu_out,
  bus
);

  input rst;
  input clk;
  input enable_a;
  input load_a;
  input [3:0] a_in;
  output [3:0] alu_out;
  output [3:0] bus;

  wire rst;
  wire clk;
  wire enable_a;
  wire load_a;
  wire [3:0] a_in;
  wire [3:0] alu_out;
  wire [3:0] bus;

  wire rstz;
  wire clken;
  wire [3:0] az;
  
  assign rstz = ~rst;
  assign clken = clk & load_a;

  assign bus = (enable_a) ? ~az : 4'bz;

  dff U_dff0
  (
    .rst (rstz),
    .clk (clken),
    .d (a_in[0]),
    .q (alu_out[0]),
    .qz (az[0])
  );

  dff U_dff1
  (
    .rst (rstz),
    .clk (clken),
    .d (a_in[1]),
    .q (alu_out[1]),
    .qz (az[1])
  );

  dff U_dff2
  (
    .rst (rstz),
    .clk (clken),
    .d (a_in[2]),
    .q (alu_out[2]),
    .qz (az[2])
  );

  dff U_dff3
  (
    .rst (rstz),
    .clk (clken),
    .d (a_in[3]),
    .q (alu_out[3]),
    .qz (az[3])
  );

endmodule
